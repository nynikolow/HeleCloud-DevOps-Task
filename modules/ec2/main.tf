# EC2 Security Group Configuration
resource "aws_security_group" "hele-ec2-sg" {
  vpc_id = var.vpc

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  } 

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }    

  tags = {
     Name = "HeleCloud-EC2-SecurityGroup"
  }
}

# Create the EFS file system
resource "aws_efs_file_system" "hele-efs" {
  creation_token = "efs-fs"
}

# Create the EFS mount target
resource "aws_efs_mount_target" "hele-efs-target" {
  file_system_id = aws_efs_file_system.hele-efs.id
  subnet_id = var.subnet_id[0]
  security_groups = [aws_security_group.hele-ec2-sg.id]
}

# Create the launch configuration for the autoscaling group
resource "aws_launch_configuration" "ec2" {
  image_id = "ami-0a261c0e5f51090b1"
  instance_type = "t2.micro"
  security_groups = [aws_security_group.hele-ec2-sg.id]
  associate_public_ip_address = true
  user_data = <<EOF
    #!/bin/bash
    sudo yum -y install httpd && sudo systemctl start httpd
    echo '<h1><center>web server exposed</center></h1>' > index.html
    sudo mv index.html /var/www/html/index.html
    sudo yum -y install amazon-efs-utils
    sudo mkdir /mnt/efs
    sudo mount -t efs ${aws_efs_file_system.hele-efs.id}:/ /mnt/efs    
  EOF
}


# Create the Auto Scaling group
resource "aws_autoscaling_group" "ec2-autoscaling" {
  launch_configuration = aws_launch_configuration.ec2.name # references ec2 launch configuration
  min_size = 2
  max_size = 3
  desired_capacity = 2
  vpc_zone_identifier = [var.subnet_id[0]]
  target_group_arns = [var.alb_target_group_arn] # attaches the autoscaling group to alb 
  tag {
    key = "Name"
    value = "HeleCloud-ec2"
    propagate_at_launch = true
  }
}


# Attach Scale-up policy to the Auto Scaling group
resource "aws_autoscaling_policy" "scale-up-policy" {
  name = "scale-up-policy"
  adjustment_type = "ChangeInCapacity"
  autoscaling_group_name = aws_autoscaling_group.ec2-autoscaling.name # autoscaling group
  policy_type = "SimpleScaling"
  scaling_adjustment = 1
}


# Create the CloudWatch alarm for high requests
resource "aws_cloudwatch_metric_alarm" "high-requests-alarm" {
  alarm_name = "High-requests"
  alarm_description = "Sends a message when the requests exceed 1000 in 30 seconds"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods = "1"
  metric_name = "RequestCount"
  namespace = "AWS/ApplicationELB"
  period = "30"
  statistic = "Sum"
  threshold = "1000"
  alarm_actions = [aws_autoscaling_policy.scale-up-policy.arn] # autoscaling policy
  dimensions = {
    LoadBalancer = var.alb_name
  }
}

