output "vpc_id" {
  value = aws_vpc.hele-vpc.id
}

output "public_subnet_ids" {
  value = aws_subnet.hele-public-subnets[*].id
}

output "private_subnet_id" {
  value = aws_subnet.hele-public-subnets[*].id
}