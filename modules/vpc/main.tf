# VPC Configuration
resource "aws_vpc" "hele-vpc" {
  cidr_block            = var.vpc_cidr_block
  enable_dns_support    = true
  enable_dns_hostnames  = true

 tags = {
    Name = "HeleCloud-VPC"
  }
}

# VPC Public Subnets
resource "aws_subnet" "hele-public-subnets" {
  count             = length(var.vpc_public_subnet_cidrs)
  vpc_id            = aws_vpc.hele-vpc.id
  cidr_block        = element(var.vpc_public_subnet_cidrs, count.index)
  availability_zone = element(var.vpc_azs, count.index)

  tags = {
    Name = "HeleCloud-VPC-Public-Subnet ${count.index + 1}"
  }
}

# VPC Private Subnets
resource "aws_subnet" "hele-private-subnets" {
  count             = length(var.vpc_private_subnet_cidrs)
  vpc_id            = aws_vpc.hele-vpc.id
  cidr_block        = element(var.vpc_private_subnet_cidrs, count.index)
  availability_zone = element(var.vpc_azs, count.index)


  tags = {
    Name = "HeleCloud-VPC-Private-Subnet ${count.index + 1}"
  }
}

# VPC Internet Gateway
resource "aws_internet_gateway" "hele-vpc-gateway" {
  vpc_id = aws_vpc.hele-vpc.id

  tags = {
    Name = "HeleCloud-VPC-InternetGateway"
  }
}

# VPC Route Table
resource "aws_route_table" "hele-rt" {
  vpc_id = aws_vpc.hele-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.hele-vpc-gateway.id
  }

  tags = {
    Name = "HeleCloud-RouteTable"
  }
}

# VPC Route Table Association
resource "aws_route_table_association" "hele-rt" {
  count           = length(var.vpc_public_subnet_cidrs)
  subnet_id       = element(aws_subnet.hele-public-subnets.*.id, count.index)
  route_table_id  = aws_route_table.hele-rt.id
}

