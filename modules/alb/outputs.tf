# output for referencing in the ec2 aws_autoscaling_group.target_group_arns
output "alb_target_group_arn" {
  value = aws_lb_target_group.hele-alb-tg.arn
}

output "alb_name" {
  value = aws_lb.hele-alb.name
}