# Security Group for the ALB
resource "aws_security_group" "hele-alb-sg" {
  vpc_id = var.vpc

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  } 

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }   

  tags = {
     Name = "HeleCloud-ALB-SecurityGroup"
  }
}


# Load Balancer Configuration
resource "aws_lb" "hele-alb" {
  name                = "hele-lb"
  internal            = false
  load_balancer_type  = "application"
  security_groups     = [aws_security_group.hele-alb-sg.id]
  subnets             = var.subnet_ids

  tags = {
    Name = "HeleCloud-ALB"
  }
}

# Load Balancer Target Group
resource "aws_lb_target_group" "hele-alb-tg" {
  name      = "hele-lb-tg"
  port      = 80
  protocol  = "HTTP"
  vpc_id    = var.vpc

  health_check {
    path                = "/"
    interval            = 30
    timeout             = 3
    healthy_threshold   = 3
    unhealthy_threshold = 3
  }

  tags = {
    Name = "HeleCloud-ALB-TargetGroup"
  }
}

# Load Balancer Listener Configuration
resource "aws_lb_listener" "hele-alb-listener" {
  load_balancer_arn = aws_lb.hele-alb.arn
  port = "80"
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.hele-alb-tg.arn
  }

  tags = {
    Name = "HeleCloud-ALB-Listener"
  }
}